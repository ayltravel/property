package com.ayltravel.property;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

	@GetMapping("/")
	public String welcomePage(final Model model) {
		model.addAttribute("title", "Property Service");
		model.addAttribute("msg", "Property Service");
		return "welcome";
	}
}
